<!-- $theme: gaia -->

# 编码风格

#### 马立珂

---

# 什么是编码风格？

* 编码风格英文叫Coding style或[Programming Style](http://en.wikipedia.org/wiki/Programming_style)
* 编码风格是书写代码的规则或指导方针。
* 它辅助程序员阅读和理解代码，以及避免写代码的时候引入错误。

---

# 编码风格包括什么？

* 缩进([Indentation](http://en.wikipedia.org/wiki/Indent_style))
* 命名规范([Naming convention](http://en.wikipedia.org/wiki/Naming_convention_(programming)))
* 空格——操作符或关键字之间
* 注释

---

# 语言与编码风格

* 大多数编程语言都有自己的编程风格；
* 这些编码风格不是通过编程书籍传播（如《Windows核心编程》，就是通过开源项目传播（如ACE，Linux Kernel等）；
* 大多数编程语言都只有1种流行的或约定俗成的风格（如Java, Ruby, Python, Perl或Erlang等）；
* 然而C却存在很多编码风格。
* 多数C++编码风格只是在已有C风格上稍做扩展

---

# 常见C/C++缩进风格

* Allman style (ANSI)
* K&R style
* GNU style

---

# ANSI style

* 由于国内早期程序员通过学习Windows(MFC)编程来学习C/C++，使用率比较高。
* 跳4或8格
* 优点: 大括号易于对齐。
* 缺点：代码块比较分散，造成一页（屏幕）内显示的有效代码非常的少

---

# ANSI style samples

```c
if (...)
{
    ...
}
else
{
    ...
}
```

```c
for (...)
{
     ...
}
```

---

# ANSI style samples

```c
while (...)
{
    ...
}
```

```c
switch (...)
{
case ...:
    break;
}
```

```c
void foo (int count)
{
    ...
}
```

---

# K&R style

* 源于Kernighan和Ritchie《C语言编程》，跳4或8格
* 多数开源代码的风格从中派生
* 优点: 代码块集中，一页所能显示的有效代码较多
* 缺点: 大括号不易对齐，然而编辑器将自动解决这个问题。


---

# K&R style samples

```c
if (...) {
    ...
} else {
    ...
}
```

```c
for (...) {
     ...
}
```

---

# K&R style samples

```c
while (...) {
    ...
}
```

```c
switch (...) {
case ...:
    break;
}
```

```c
void foo (int count)
{
    ...
}
```

---

# GNU style

* Richard Stallman推广，广泛被GNU的开源项目所使用
* 跳2格，具体参看GNU Coding Standards

---

# GNU style samples

```c
if (...)
  {
    ...
  }
else
  {
    ...
  }
```

```c
for (...)
  {
    ...
  }
```

---

# GNU style samples

```c
while (...)
  {
    ...
  }
```

```c
switch (...)
  {
    case ...:
        break;
  }
```

```c
void foo (int count)
{
    ...
}
```

---

# 缩进辅助工具

* [astyle](http://astyle.sourceforge.net/)，跨平台，支持C/C++，不断在更新，强烈推荐
* [indent](https://www.gnu.org/software/indent/)，仅支持C语言
* [bcpp](http://invisible-island.net/bcpp/)，支持C/C++，然而早已不更新

---

# 常见C/C++命名规范

* 匈牙利命名法(Hungarian notation)
* Java变量命名法
* 下划线的命名法

---

# 匈牙利命名法

* **优势**：基本类型的变量类型一目了然
* **劣势**：
    * 变量名过长，传递的信息冗余
    * 复杂类型的变量缺乏命名规范，特别是自定义类型的变量，破坏可读性，给程序员造成困扰
* 早已被业界**摒弃**，特别是开源代码，甚至M$的.NET
* 国内依旧有部分用户，主要**原因**：
    * 多数国内C/C++程序员从MFC开始学习C/C++
    * 早期Visual C++ 5/6的IDE漏洞百出，对代码阅读支持不好。

---

# 匈牙利命名法

**Linus Torvalds**: Encoding the type of a function into the name (so-called Hungarian notation) is brain damaged—the compiler knows the types anyway and can check those, and it only confuses the programmer.

---

# 匈牙利命名法

**Bjarne Stroustrup**: No I don't recommend 'Hungarian'. I regard 'Hungarian' (embedding an abbreviated version of a type in a variable name) a technique that can be useful in untyped languages, but is completely unsuitable for a language that supports generic programming and object-oriented programming—both of which emphasize selection of operations based on the type an arguments (known to the language or to the run-time support). In this case, 'building the type of an object into names' simply complicates and minimizes abstraction.

---

# Java变量命名法

* **常量**：若干个大写单词通过下划线连接而成，如`MAX_AGE`
* **变量**和**函数**：小写驼峰词，如`firstName`
* **类**：大写驼峰词，如`CodingStyle`
* 源于Java编程规范，广泛用于开源代码

---

# 下划线的命名法

* **常量**：若干个大写单词通过下划线连接而成，如`MAX_AGE`
* **变量**、**函数**和**名字空间**：若干个小写单词通过下划线连接而成，如`age`, `first_name`
* 在STL和Boost库中，**类名**由若干个小写单词通过下划线连接而成，如`auto_ptr`
* 在ACE中，**类名**由首字母大写的单词通过下划线连接而成，如`ACE_Reactor`
* 广泛用于各种开源代码

---

# 空格

* 一元操作**没有**空格，如`++i`和`i++`
* 二元操作符(`+`, `-`, `*`, `/`, ...)左右各**1**个空格，如`a + b`
* 关键字`if`/`for`/`while`/`switch`与`(`之间**1**个空格，如`if (...)`
* ANSI和K&R风格：函数名与`(`之间**没有**空格
* GNU风格：函数名与`(`之间**1**个空格

---

# C/C++注释规范

* 越来越多的开源代码采用doxygen风格的注释
* 优势：
    * 支持多种开发语言
    * 通过doxygen工具生成代码的接口文档
    * 便于接口代码阅读
    * 便于实现代码审阅和学习

---

# Doxygen

* 具体规范请看[Doxygen Manual](http://www.stack.nl/~dimitri/doxygen/manual.html).
* 一般掌握3种规则，就足以满足常规工作需要
    * 头文件注释
    * 函数注释
    * 成员变量注释

---

# Doxygen

```cpp
/// \file device_manager.h
/// \brief Device manager is the device container.
/// \author Like Ma <likemartinma@gmail.com>
#ifndef DEVICE_MANAGER_H
#define DEVICE_MANAGER_H

class Device_Manager {
public:
    /// \brief Get disk object by device number.
    /// \param[in] num The disk device number.
    /// \return The disk object.
    Device* device(dev_t num) const;

```

---

# Doxygen

```cpp
    /// \brief Process the device in thread safe mode.
    /// \param[in] id The device device number.
    /// \param[inout] walker The device processor.
    /// \retval -1 denotes the device not found
    ///            or \c walker is null.
    /// \retval 0 denotes OK.
    int walk_device(dev_t id,
                    Device_Walker* walker) const;
protected:
    int cdrom_count_; ///< the count of CD-ROM drive

    /// Use bit vector from lower bit to higher bit
    /// to store drive letter from 'a' to 'z'.
    unsigned letter_bits_;
};

#endif // DEVICE_MANAGER_H
```

---

# 我们使用什么风格？

* C/C++：
    * 缩进：K&R style，空4格
    * 命名：下划线命名
    * 空格：K&R style
    * 注释：Doxygen
* Java： [Java编码规范](http://www.oracle.com/technetwork/java/codeconv-138413.html)
* Python: [Style Guide for Python Code](http://www.python.org/dev/peps/pep-0008/)
* 其它语言，可以遵循其对应的编码规范
* 维护代码时，尽可能保持与原来风格一致。
* 最基本的原则是保持一致，至少单个文件中的风格必须保持一致。


---

# 建议

* 良好的编码风格是最基本的专业技能。
* 良好的变量命名是专业素养的良好体现。
    * 较短且意思明确的单词，最好是专业术语
    * 较短的单词缩写，最好是专业术语缩写
    * 阅读专业英文资料（包括教材、论文和新闻），掌握更多专业术语
    * 阅读开源代码，掌握更多约定俗的术语及缩写
